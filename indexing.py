import os
import pickle

def indexing (inputpath):
    term_dict = {}
    origin_files = [file for file in os.listdir(inputpath)]

    filecount = 0
    # Construct the invert index
    for filename in origin_files:
        # print(filecount)
        file = open(inputpath + filename, 'r',encoding='UTF-8')
        text = file.read().split()
        file.close()
        for word in text:
            if word in term_dict:
                term_item = term_dict[word]
                if filecount in term_dict:
                    term_item[filecount] += 1
                else:
                    term_item[filecount] = 1

            else:
                term_dict[word] = {filecount: 1}
        filecount += 1

    # return origin_files, term_dict, idf_dict

    # Save as structures
    f = open('file_list.dat', 'wb')
    pickle.dump(origin_files, f)
    f.close()

    f = open('posting_list.dat', 'wb')
    pickle.dump(term_dict, f)
    f.close()
