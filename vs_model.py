import pickle
from math import log10, sqrt
from processing import implStopwordStemmer

# load structure
def get_index():
    with open("posting_list.dat",'rb') as f:
        index = pickle.load(f)
    with open("file_list.dat", 'rb') as f2:
        file_list = pickle.load(f2)

    return index, file_list

def length(vector):
    length = float(0)
    for num in vector:
        length += num*num
    return sqrt(length)

# def similarity(w1,w2):
#     sim = float(0)
#     for wi1, wi2 in zip(w1, w2):
#         sim += wi1*wi2
#     sim = sim/(vectorlength(w1)*vectorlength(w2))
#     return sim


# Get candidates docs
def combine_doclist(candidates):
    if len(candidates):
        result = candidates[0]
        for num in range(1, len(candidates)):
            result = [i for i in result if i in candidates[num]]
        return result
    else:
        return []

# Compute the idf for each term
def get_idf(index):
    idf_dict = {}
    N = len(index)
    for word, doclist in index.items():
        idf_dict[word] = log10(N / len(doclist))  # idf = log_10(N/df)

    f = open('idf_dict.dat', 'wb')
    pickle.dump(idf_dict, f)
    f.close()
    return idf_dict

# Compute the vectors' length
def get_vectorlength(vectors):
    vector_length = []
    for docv in vectors:
        vector_length.append(length(docv))

    f = open('vector_length.dat', 'wb')
    pickle.dump(vector_length, f)
    f.close()
    return vector_length

# Compute the doc vectors
def get_vectors(index,file_list, idf_dict):
    vectors = [[] for i in range(len(file_list))]
    for word, doclist in index.items():
        idf = idf_dict[word]
        for doc, freq in doclist.items():
            vectors[doc].append(freq*idf)
    return vectors

# Compute the values that a vs_model need
def vs_model_pre():
    index, file_list = get_index()
    idf_dict = get_idf(index)
    docv_length = get_vectorlength(get_vectors(index,file_list, idf_dict))
    return index, file_list, idf_dict, docv_length

# Processing query, unfinished
def processing_query(index, file_list, idf_dict, docv_length, query):
    # Pre-process the query with the stop list and stemmer
    stem_query = implStopwordStemmer(query)

    doclists = []
    for item in stem_query:
        doclists.append(index[item].keys())
    candidates = combine_doclist(doclists)
    # Candidates:candidate documents that contain at least one query term
    print(candidates)

    #compute the length of the query vector.
    # get lists od w1,w2
    #compute the TF-IDF similarity score between the query and each candidate document
