from processing import processing
from indexing import indexing
from vs_model import vs_model_pre,processing_query

origin_dir = "./docsnew/"
processed_dir = "./processed_data/"

processing(origin_dir, processed_dir)
indexing(processed_dir)
vs_model_pre()